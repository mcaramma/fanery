.. Fanery documentation master file, created by
   sphinx-quickstart on Sun Aug 31 13:49:54 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fanery's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   fanery
   intro
   hardened-system
   backend-web-farm
   working-with-fanery
   tutorial
   security-protocol
   storage


.. Indices and tables
   ==================

.. * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

