from fanery import (
    service, static, get_state, auth,
    DataStore, dbm, server, wsgi, config
)
from sys import argv

if ':debug' in argv:
    import logging.config
    logging.config.dictConfig(config.LOGGING)

if ':with-dsstore' in argv:
    ds = dbm.DSStore()
else:
    ds = dbm.MemDictStore()

storage = DataStore(storage=ds,
                    permission=ds,
                    state=ds,
                    abuse=ds,
                    profile=ds,
                    settings=ds)
auth.setup(storage)

if ':init' in argv:

    if ':domain' in argv:
        domain = argv[argv.index(':domain') + 1]
    else:
        domain = 'localhost'

    if ':username' in argv:
        username = argv[argv.index(':username') + 1]
    else:
        username = 'myadmin'

    if ':password' in argv:
        password = argv[argv.index(':password') + 1]
    else:
        password = 'mysecret'

    auth.add_user(username, password, domain=domain)

ssl = bool(':ssl' in argv)

if ':static-dir' in argv:
    static_dir = argv[argv.index(':static-dir') + 1]

    if ':static-dir-urlpath' in argv:
        static_dir_urlpath = argv[argv.index(':static-dir-urlpath') + 1]
    else:
        static_dir_urlpath = '/'

    static(static_dir_urlpath, static_dir, ssl=ssl)

if ':app' in argv:
    execfile(argv[argv.index(':app') + 1])
else:
    @service(ssl=ssl, safe=False, auto_parse=False)
    def hello(name='World'):
        return 'Hello, %s!' % name

    @service(ssl=ssl, safe=False, auto_parse=False)
    def exc(*args, **argd):
        raise Exception('expected Exception')

    @service(ssl=ssl)
    def error(*args, **argd):
        raise Exception('expected Exception')

    @service(ssl=ssl, auto_parse=False)
    def profile():
        profile = get_state().profile
        return profile._indexer(profile)

    @service(ssl=ssl, safe=False, auto_parse=False)
    def upload(uploads):
        from os.path import getsize, basename
        return dict((basename(path), getsize(path)) for path in uploads)

    @service(ssl=ssl, safe=False, static=True, cache=True,
             force_download=True, content_disposition=True)
    def download(name):
        from os.path import join, basename
        from fanery import config
        return join(config.UPLOAD_DIRPATH, basename(name))

    if not ssl:
        static('/jfanery', config.JFANERY_DIRPATH, ssl=ssl, force_define=True)

params = dict(ssl=ssl,
              gevent=bool(':gevent' in argv),
              bjoern=bool(':bjoern' in argv),
              cherrypy=bool(':cherrypy' in argv),
              profile=bool(':profile' in argv))

if ':host' in argv:
    params.update(host=argv[argv.index(':host') + 1])

if ':port' in argv:
    params.update(host=argv[argv.index(':port') + 1])

if ':start' in argv:
    server.start_wsgi_server(**params)
else:
    app = wsgi.build_handler(**params)
