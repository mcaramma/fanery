#!/bin/sh

LIBFILE=fanery/jfanery-lib.js

cat > $LIBFILE <<EOF
// combined third party javascript libraries minified with slimit
// https://raw.github.com/tonyg/js-nacl/master/lib/nacl_factory.js
// https://raw.github.com/tonyg/js-scrypt/master/browser/scrypt.js
// https://raw.github.com/douglascrockford/JSON-js/master/json2.js
// https://raw.github.com/dankogai/js-base64/master/base64.js
// https://code.jquery.com/jquery.js
EOF

wget -O - https://raw.github.com/tonyg/js-nacl/master/lib/nacl_factory.js \
	  https://raw.github.com/tonyg/js-scrypt/master/browser/scrypt.js \
	  https://raw.github.com/douglascrockford/JSON-js/master/json2.js \
	  https://raw.github.com/dankogai/js-base64/master/base64.js \
	  https://code.jquery.com/jquery.js \
	  >> $LIBFILE
	  #| slimit -m -t >> $LIBFILE

cat fanery/jfanery.js >> $LIBFILE
