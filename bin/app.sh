#!/bin/sh

PID_FILE=${PID_FILE:-/var/run/fanery.pid}
WSGI_APP=${WSGI_APP:-app:app}
BIND=${BIND:-0.0.0.0}
PORT=${PORT:-9000}
BACKLOG=${BACKLOG:-2048}
LOG_LEVEL=${LOG_LEVEL:-debug}
WORKERS=${WORKERS:-$(($(grep ^processor /proc/cpuinfo | wc -l) * 2 + 1))}
PYTHONPATH="${PYTHONPATH:-./}"

case "$1" in
    start)
        PYTHONPATH="$PYTHONPATH" rainbow-saddle \
            --pid $PID_FILE gunicorn $WSGI_APP \
            --workers=$WORKERS -b $BIND:$PORT \
            --backlog $BACKLOG --log-level $LOG_LEVEL
        exit $?
        ;;
    reload)
        kill -HUP $(cat $PID_FILE)
        exit $?
        ;;
    stop)
        kill -TERM $(cat $PID_FILE)
        exit $?
        ;;
    *)
        echo "Usage: $0 {start|reload|stop}"
        exit 1
        ;;
esac

# read http://gunicorn-docs.readthedocs.org/en/latest/deploy.html
