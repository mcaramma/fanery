#!/bin/sh

FANERY_DIR="$(dirname $0)/.."

PYTHONPATH=$FANERY_DIR python \
    $FANERY_DIR/wsgi_app.py :start \
    :host 192.168.122.151 :init :domain 192.168.122.151 \
    :static-dir $FANERY_DIR/docs/_build/html \
    :static-dir-urlpath /docs :debug $@
